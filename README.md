# Notebook

This is my digital notebook where I keep all of my notes and exercise solutions from all kinds of online courses and resources.

Web UI for my note book is [here](https://notebook-client.herokuapp.com/)
