# CompSci Syllabus

The syllabus divides into three sections. 1 - is for Computer Science guide. 2 - Doing a fullfilled project. 3- Teaching the market skills for myself. Write a feynman note for each check box learned.

## Computer Science Guide

Try to make at least something in each section and since sections put on top of one another do not move to the other section without learning the perivious one fully.

### Programming

Don’t be the person who “never quite understood” something like recursion

 - [ ] Course : [Helsinki MOOC's](http://mooc.fi/courses/2013/programming-part-1/material.html) for the desired language.
 - [ ] Book : [Structure and Interpretation of Computer Programs](http://web.mit.edu/alexmv/6.037/sicp.pdf). Just read the first 3 chapters.
 - [ ] Videos : [Brian Harvey’s Berkeley CS 61A](https://archive.org/details/ucberkeley-webcast-PL3E89002AA9B9879E?sort=titleSorter)
  
### Computer Architecture
  
  If you don’t have a solid mental model of how a computer actually works, all of your higher-level abstractions will be brittle.
  
  - [ ] Course : [Nand2Tetris Part I](https://www.coursera.org/learn/build-a-computer)
  - [ ] Course : [Nand2Tetris Part II](https://www.coursera.org/learn/nand2tetris2)
  - [ ] Book : [Computer Organization and Design](http://mprc.pku.edu.cn/courses/organization/autumn2012/COD.pdf). Not every section is essential.
  - [ ] Course : [CS61C course](http://inst.eecs.berkeley.edu/~cs61c/sp15/) past lecture [videos](https://archive.org/details/ucberkeley-webcast-PL-XXv-cvA_iCl2-D-FS5mk0jFF6cYSJs_) are here
  
  ### Algorithms & Data Structures
  
  If you don’t know how to use ubiquitous data structures like stacks, queues, trees, and graphs, you won’t be able to solve hard problems.
  
  - [ ] Book: [The Algorithm Design Manual](https://www.amazon.com/Algorithm-Design-Manual-Steven-Skiena/dp/1848000693/?pldnSite=1) There are [vidoes](http://www3.cs.stonybrook.edu/~algorith/video-lectures/) avilable too.
  - [ ] Course: [Stanford MOOC](https://www.coursera.org/specializations/algorithms)
  - [ ] Exercise : [LeetCode](https://leetcode.com/) - solve around 100 problems.
  - [ ] Book : [How to solve it](https://www.amazon.com/How-Solve-Mathematical-Princeton-Science/dp/069116407X/?pldnSite=1)
  - [ ] Course : [Algorithms part I](https://www.coursera.org/learn/algorithms-part1)
  - [ ] Course : [Algorithms part II](https://www.coursera.org/learn/algorithms-part2)
  
### Mathematics for Computer Science
  
  CS is basically a runaway branch of applied math, so learning math will give you a competitive advantage.
  
  - [ ] Study : Most important math for CS is discrete math. Finish the prerequisites from khan academy.
  - [ ] Course : [Important dicrete course](https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-042j-mathematics-for-computer-science-fall-2010/video-lectures/)
  - [ ] Read : [Lecture notes for Discrete Mathematics](http://www.cs.elte.hu/~lovasz/dmbook.ps)
  - [ ] Course : [Mathemtaics for Computer Scinece](https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-042j-mathematics-for-computer-science-spring-2015/index.htm)
   - [ ] Course: [Essence of Linear Algebra](https://www.youtube.com/playlist?list=PLZHQObOWTQDPD3MizzM2xVFitgF8hE_ab)
   - [ ] Course : [Linear Algebra](https://ocw.mit.edu/courses/mathematics/18-06-linear-algebra-spring-2010/video-lectures/)
   
 ### Operating Systems
   
   Most of the code you write is run by an operating system, so you should know how those interact.
   
   - [ ] Book : [Operating Systems: Three Easy Pieces](http://pages.cs.wisc.edu/~remzi/OSTEP/)
   - [ ] Course : [ops-class.org : hack the kernel](https://www.ops-class.org/) You can go with the book simotenously.
   - [ ] Project : Code a small OS kernel
   
 ### Computer Networking
   
   The Internet turned out to be a big deal: understand how it works to unlock its full potential.
   
   - [ ] Book : [Computer Networking: A Top-Down Approach](https://www.amazon.com/Computer-Networking-Top-Down-Approach-7th/dp/0133594149/?pldnSite=1) -- do the exersices
   - [ ] Exersices : [Wireshark](http://www-net.cs.umass.edu/wireshark-labs/)
   - [ ] Course : [Stanford MOOC](https://lagunita.stanford.edu/courses/Engineering/Networking-SP/SelfPaced/about)
   - [ ] Project : Your own small internet, HTTP server or etc. dont forget to practice what you learn.
   
 ### Databases
   
   Data is at the heart of most significant programs, but few understand how database systems actually work.
   
   - [ ] Course : [Berkley Course](https://archive.org/details/UCBerkeley_Course_Computer_Science_186)
   - [ ] Paper : [Database Architecture](http://db.cs.berkeley.edu/papers/fntdb07-architecture.pdf)
   - [ ] Book : [Red Book](http://www.redbook.io/) -- your next stop after berkley course
   - [ ] Course : [Stanford Course](https://lagunita.stanford.edu/courses/DB/2014/SelfPaced/about) -- Take all mini courses
   - [ ] Project idea: Write a simple relational database management system from scratch.
   
   ### Languages and Compilers
   
   If you understand how languages and compilers actually work, you’ll write better code and learn new languages more easily
   
   - [ ] Course : [Stanford Course](https://lagunita.stanford.edu/courses/Engineering/Compilers/Fall2014/about) read the [dragon book](https://www.amazon.com/Compilers-Principles-Techniques-Tools-2nd/dp/0321486811?pldnSite=1) on the units of stanford do not read all the book.
   - [ ] Project : Create a basic compiler for a learning langauge or a simple language you have created.
   
 ### Distributed Systems
   
   These days, most systems are distributed systems.
   
   - [ ] Book : [Distibuted Systems, 3rd edition](https://www.distributed-systems.net/index.php/books/distributed-systems-3rd-edition-2017/)
   
### Code University

  - [ ] Guide : [Code Univeristy Guide](https://github.com/jwasham/coding-interview-university) : Finish this guide after finishing the sections above this guide suppose to take your time nearly a year so go through each item very seriously. If you are able to come this far hell go apply for big 4.
   
 ### Other Pet/Advanced Topics
 
 - [ ] Course : [Software Debugging](om/course/software-debugging--cs259)
 - [ ] Course : [Software Testing](https://www.udacity.com/course/software-testing--cs258)
 - [ ] Course : [Software Architecture & Design](https://www.udacity.com/course/software-architecture-design--ud821)
 - [ ] Course : [Agile Software DEvelopment](https://www.edx.org/course/agile-software-development)
 - [ ] Course : [Intro to Cloud Infrastructure Technoliges](https://www.edx.org/course/introduction-cloud-infrastructure-linuxfoundationx-lfs151-x)
 - [ ] Course : [Intro to Cyber Security](https://www.edx.org/course/introduction-cloud-infrastructure-linuxfoundationx-lfs151-x)
 - [ ] Course : [Software Engineering(introcution)](https://www.edx.org/course/software-engineering-introduction-ubcx-softeng1x)
 - [ ] Course : [Parallel Computer Architecture and Programming](http://15418.courses.cs.cmu.edu/spring2016/home)
 - [ ] Course : [IoT](https://www.coursera.org/specializations/internet-of-things)
 - [ ] Course : [Computer Graphics](https://www.edx.org/course/computer-graphics-uc-san-diegox-cse167x-3)
 - [ ] Course : [Artifical Intelligence](http://www.elementsofai.com/)
 - [ ] Course : [Machine Learning](https://www.coursera.org/learn/machine-learning)
 - [ ] Course : [Natural Language Processing](https://www.coursera.org/learn/natural-language-processing)
 
 There are a lot more online free courses check OSS University or Other starred guide repos ...
 
 ---
 ---
 
 ## Fully Developed Project
 
 You should maintain one big application instead of writing new clones everyday.
 
- [X] Project : Start maintaining and commiting your time to `Spotlight`
  - [ ] See Medium if there is useful mechanism that can help you with the 'spot' image and file uplloading to the posts.
  - [ ] See Reddit if there is useful mechanism that can help you with the 'spot's user groupings and sub channels
  - [ ] See Netflix if ther eis usefl mechanism that you can help with your file uploading to spot
  - [ ] See Slack if there is ueful mechanism that can help you with the spots chatting system.
  - [ ] See duolingo if there are useful mechanism that can help with spot
  - [ ] See Facebook for useful features that you might have missed since there a lot in facebook notifications, etc.
  - [ ] Definietly check github/gitlab features like analytics .. etc.
  - [ ] Definetly check Jira for features like to dos, analytics, deadlines .etc.
- [ ] Project: Create bots to create content. In oder to do that you need to learn how to create a API for hobio
- [ ] Project: Learn how to deal with the PaaS, IaaS host services for your projects 'hobio' in this instance.
- [ ] Project: Keep maintaining Spotligth it will be never done it is your open source commitment from now on.
- [ ] Project: Update your Portfolio highlighting your best work. Make it professional you can get insparation from hipolabs.com they illustrate a very good example of freelanceers website
- [ ] Project: Finish any questions you have remaining from Python Interview Prep
- [ ] Project: Create and polish your Resume/CV.
- [ ] Project: [Cracking the Coding Interview](https://www.hackerrank.com/domains/tutorials/cracking-the-coding-interview) on HackerRank
- [ ] Project: Try to sell a frontend template code. Or atleast get your foot in the door for freelancing. 
- [ ] Important: **Start Contributing to Open Source projects**
  - [ ] [First Contributions](https://github.com/Roshanjossey/first-contributions)
  - [ ] [Open Source Guide](https://github.com/github/opensource.guide)
  
  - [ ] Start earning money from what you code since you completed your fundemental basic education spend your time more wisely like contributing to big remote working friendly companies open source projects. 
 - [ ] You can try to win [kaggle.com](https://www.kaggle.com/) prices since there are really good prices such as up to 1 million dollars
  
  
 ---
 ---
 
 
 ## Market Skills Road Map
 
 It is important to learn in this sequence 1-Intro, 2-Backend, 3-Frontend, 4-DevOps. And do not try to build atleast one thing after learning each subject.
 
 ### Intro 
 These are the basic and essential skills that every developer shoould have it under their belt in todays market. Try to practice what you learn.
 
 - [X] Git - Version Control
 - [ ] SSH
 - [ ] HTTP/HTTPS
 - [ ] API's
 - [X] Basic Terminal Usage
 - [ ] Learn to research
 - [ ] Data Structures and Algorithms
 - [ ] Character encodings
 - [ ] Design Patterns
 - [X] GitHub for public repos, GitLab for private
 
 ### Front End
 This may seem basic since in the core there are only three technologies how ever what is really hard is that not everyone has the eye for killer design skills.
 
 - [X] Learn : HTML
 - [X] HTML : Clone a template content
 - [X] Learn : CSS
 - [ ] CSS : Learn how to use 'grid' and 'flexbox'
 - [ ] CSS : Learn Media Queries and Responsive Design
 - [X] CSS : Style the template you made above
 - [X] Learn : JavaScript
 - [X] JS : Learn the DOM api and how to manipulate it
 - [ ] JS : finish YDKJS books for understanding hoisting, bubling ... etc.
 - [ ] JS : Learn Ajax(XHR)
 - [ ] JS : Learn ES6 and writing modular javascript
 - [ ] JS : Optionally you can learn Jquery
 - [ ] Project : Make a fine Responsive website with interactivity with javascript
 - [ ] JS : Pick a package manager and learn it, either `npm` or `yarn`
 - [ ] CSS : Learn SASS a css proprecessor that lets you do things css can't do it by it self
 - [X] CSS : Learn BootStrap a framework for responive design
 - [ ] CSS : Learn BEM, a css architecture to stucture your css code 
 - [ ] Learn : Build tools in this order, 1: NPM scripts, 2: ESLint, 3:Webpack
 - [ ] Project : Create a library and publish it on npm and github
 - [ ] Learn : Pick a framework from this three, 1:React--redux, 2:Vue--vuex, 3:Angular2--Rx.js
 - [ ] Project : You now know what a modern javascript developer should now. Go build a fully developed front end application.
 - [ ] Learn : How to imporove the performance of your application built above such as, eg. Interactivity time, Page speed index, Lighthouse Score.
 - [ ] Learn : Testing your front end applications [here is a good resource](https://medium.com/welldone-software/an-overview-of-javascript-testing-in-2018-f68950900bc3)
 - [ ] Learn : To make progressive web apps, you need to learn about service workers to make them
 - [ ] Learn : Static Type Checkers, you dont need to learn them but they give you overrpower of your code, `TypeScript` is a good option
 - [ ] Learn : Server side rendering in whatever framework you have chosen. If it is 1:react -- Next.js, 2:vue -- Nuxt.js, 3:Angular2 -- Universal
 - [ ] ETC : There are lots of things to learn go figure such as, Canvas, HTML5 APIs, SVG, Functional Programming .. etc.
 
 ### Back End
 It may seem daunting compared to front-end however and yes it is true that there is a learning curve. However it is a lot more fun to be able to understand what the fuck is going under all of this complexity.
 
 - [X] Learn : Pick a language for backend you may chose Scripting languages: Python, Ruby, PHP, Node or Functional languages: Haskell, Scala .. etc. or Multiparadigm langauges: Go, Rust or Enterprisey languages: Java, C#
 - [X] Practice: Exercise and make some command line application. Sample ideas, you can make a web scraper or get a API in JSON format and create someting, think of a daily task and automate it.
 - [X] Learn: Learn how to use a package manager for the language you have picked e.g. Python has pip etc.
 - [X] Learn: Standards and best practices. Each language has it is own e.g. Python has PEP
 - [X] Projeect: Make and Distribute some package / Library.
 - [X] Learn: Learn about testing. There are several ways to write tests such as unit , integration ..etc. Tests are very important do not skim through articles. Learn how to calculate test covarege.
 - [X] Project: Write tests for your package built above.
 - [ ] Learn Relational Databases there are many options but pick either PostgreSQL or MySQL
 - [ ] ProjectCreate a simple CRUD web applciation like a blog or something using everything you have learnt this far. It should have a registration, adminpanel, posts ...etc. You can use [Werkzeug](http://werkzeug.pocoo.org/) for building your site.
 - [X] Learn: a Framework e.g. Django, Flask
 - [ ] Project: Make the application you made above with the framework you chose.
 - [ ] Learn: a noSQL database you do not have to use it but understand how it is different from relational databases and why they are needed. MongoDB is a solid choice.
 - [ ] Learn: Caching, learn how to implenet app level caching using Redis or Memcached
 - [ ] Project: Implement caching to the application you have built above.
 - [ ] Project: Understand REST and learn how to make RESTful APIs and make sure you read about REST from the original paper of Roy Fielding
 - [ ] Learn: Authentication/Authorization methodiolgies. There are many choices such as OAuth, Token Auth, JWT ..etc. you can choose once you understand more about authentication.
 - [ ] Learn: about Message Brokers, understand the "why?" and pick one. There are multiple options but RabbitMQ and Kafka is solid options.
 - [ ] Learn: As the application grows simple queries on your database arent going to cut it out and you will have to resort to a search engine. There are many options like Solr, ElasticSearch pick one after you learn about the topic more.
 - [ ] Learn: How to use Docker
 - [ ] Learn: Knowledge of Web Servers, There are several different options look at the differnt options understand differneces and limitations. Apache, Nginx .. etc.
 - [ ] Learn: how to use Web Sockets.
 - [ ] Project: Make a very big application for learning and build it from scratch with even building your own server and using web sockets to accsess it. Or you can simply build a web framework built on top of wsgi or something else.
 - [ ] Learn: GraphQL, this is not required and optional feel free to have a loot and what it is all about and why they are calling it the new REST
 - [ ] Learn: Again option, look into Graph Databases, at least you can have a little udnerstanding of what they are.
 - [ ] We have a lot more stuff like Profiling, Static Analysis, DDD SOAP, etc. Go on your journey now!

### DevOps
DevOps is something very complex instead of having a one strict route you cna carve your way up however it is very important to have a mental map of dev ops that consists of these. Try to understand each topic and atleast do a small lite demo of the things you like for practice.

- [ ] Learn: Operating Systems, Either unix -> FreeBSD or Linux --> RHEL || CentOS || Ubuntu || Fedora
- [ ] Learn: Automation, Either powershell, Puppet, AWS, Terraform .. etc.
- [ ] Learn: Cloud, Either AWS, Heroku, DigitalOcean ... etc.
- [ ] Learn: CI/CD , Either Jenkins, Travis, CircleCI .. etc.
- [ ] Learn: Monitoring and Alerting, Either, AppDynamics, Naglos, Munin, New Relic ...etc.
- [ ] Learn: Log Management & Analysis, Either ELK ...etc.
- [ ] Learn: Cluster Managers, Either Kubernetes, Nomad, Docker Swarrm .. etc.
- [ ] Learn: Love for Terminal, learn compiling apps from source, gcc and etc.
- [ ] Learn: Bash Scripts
- [ ] Learn: Vim/Nano/Emacs
- [ ] Learn: Command Tools, Text Manipulation (awk, sed, grep, sort, uniq, cat, echo ..etc), Process Monitoring(ps, top, htop,atop ..etc), System Performance(nmon, iostat, sar ..etc), Network(nmap, tcdump, ping, mtr, traceroute ..etc)
- [ ] Learn: Containers, Either Docker ...etc.
- [ ] Learn: Web Servers, Either Apache, Nginx, Tomcat .. etc.
- [ ] Learn: OSI Model. TCP/IP/UDP Common ports
- [ ] Learn: Knowledge about different file systems.
- [ ] Learn: Setting up a Reverse Proxy(Nginx)
- [ ] Learn: Setting up a caching Server(Squid, Nginx)
- [ ] Learn: Setting up a load balancer(HAProxy, Ngixns)
- [ ] Learn: Setting up a firewall
- [ ] Learn: TLS, STARTLS, SSL, HTTTPS, SCP, SSH, SFTP
- [ ] Learn: Postmortem analysis when something bad happens.

 
### Visual Road Map
 
 <img align="center" width="100%" src="/images/intro.png" />
 <img align="center" width="100%" src="/images/frontend-v2.png" /> 
 <img align="center" width="100%" src="/images/backend.png" />
 <img align="center" width="100%" src="/images/devops.png" />
 
 
 
 

 
   
   
   
   
