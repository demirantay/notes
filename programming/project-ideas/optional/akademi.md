[Akademi.com]() is a platform for scholars and artists or scientists that aims to protect and develop information.


## What is the idea?

This project originates from the ancient academy called 'Akademia' which was founded by Plato in 387 BC in Athens. Academy was not a school in which an orthodox metaphysical doctrine was taught or an association of members who were expected to subscribe to the theory of ideas ... The metaphysical theories of the director were not in any way 'official'.

My main motive is to immigrate this ancient system of preserving and sharing information to a virtual world where all the scholars around the globe can share their knowledge.

## Development Road-Map

Road map of the mechanisms that I will add to the platform.

P.S. I am quite a beginner in programming. So I will take my time while building this project since I am learning at the same time.

### Version 1.0

- [ ] Sign up
- [ ] Log in
- [ ] Log out
- [ ] Personal Page
  - [ ] Portfolio Picture
  - [ ] Bio
  - [ ] Followers (disciples)
  - [ ] Following (Mentors)
  - [ ] Archives aligned by categories and date
  - [ ] Self-entries / papers (That is like github repos but dots(archive system(classes)) are 'science', 'music')
- [ ] Arguments, commenting/discussion system (This is going to be at the every self-entries/papers and will look like the argument mappings in arguman.org. However instead of using keywords in arguman.org such as `but` `or` `and` use `I agree because` and `I disagree because`)
 - [ ] Classes (categories)(when clicked displays only the user-entries that its category is same with that specific class)
 - [ ] Home Feed (if not logged in shows the most popular entires of the week/ if logged in shows the latests and the most popular entries of your following, and can toggle to the most popular views )

### Version 2.0

- [ ] Expand Classes (I have no ideas yet I will add this later ...)
- [ ] Library (Alexandria, Freebooks courses... I have no ideas will add later... )

Will add more later...

## Contributors

- [Demir Antay](https://github.com/demirantay)
