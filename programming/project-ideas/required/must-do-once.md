Every programmer should at least create, at least once:

- [ ] Operating System
- [ ] Editor
- [ ] Database
- [ ] RogueLike
- [ ] Interpreter
- [ ] Compiler
- [ ] A Robot
