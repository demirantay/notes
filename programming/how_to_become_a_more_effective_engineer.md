These are the notes taken from the book 'The Effective Engineering' by Edmond Lau.

<h3>What is an effective enginner ? </h3>
They are people who get things done. Effective engineers produce results.

<h2>Adopt the right mindsets</h2>

<h3>Focus on high leverage activities</h3>
- Leverage = Impact produced / Time invested
- Use your leverage as your yardstick for effectiveness. 80% of the impact comes from 20% of the work & always focus on the leverages that will teach you skills. Never go for wins just because they are easy. Always challenge yourself.

<h3>Optimize for learning</h3>
- Change jobs if you have to. Do not stay at a job just because it is easy but don't change your job because you're bored they are not the same thing.
- Optimizing(using the best of what do you have) for learning is high leverage.
- Adopt a growth mindset. Those with a growth mindset believe that they can cultivate and grow their intelligence and skills through effort.
- Invest in learning. Working on unchallenging tasks is a huge opportunity cost. You missed out on a learning opportunity. Prioritize learning over profitability. Invest your time in activities with the highest learning rate.
- Seek work environments stimulating your learning. Make sure you are working on high priority projects in your work environment. Look for culture with curiosity, where everyone is encouraged to ask questions and you are surrounded by people that are smarter than you.
- While on the job make a daily habit of acquiring new skills. Read code written by brilliant engineers. Jump fearlessly into code you do not know. Always be learning. Invest in skills that are in high demand. Build and maintain strong relationships.

<h3>Prioritize Regularly</h3>
-Always prioritize on high leverage activities do not set anchor your growth by years simply working on wrong ideas.
-Maintain single todo lists where all tasks are listed. Do not overcomplicate the list. Don't try to remember everything. The brain is bad at remembering it's rather good at processing that's why there is a reference.
- Ask yourself regularly: Is this the most important thing I should be working on?
- Focus on what directly produces value and learn to say no. While working find ways to get into the flow. "A state of effortless concentration so deep that they lose their sense of time, of themselves, of their problems."
- Always limit the amount of work in progress. Cost of context switching is a terrible price.
- Prioritizing is a very hard task. Think about as versions of your projects. What would be the thing that you prioritize yourself that would be the best update in the version of yourself?

<h3>Balance quality</h3>
- High code quality is code that has taste and is read very well.
- Establish sustainable code review process. Always be open to feedback and grow a thick skin for toxic people.
