class Multiplier:

    #consturctor
    def __init__(self, number):
        self.number = number

    #behaviors
    def multiply(self, other_number):
        return self.number * other_number
