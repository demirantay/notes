user_grade = int(input('type your grade: (1-100) '))

if user_grade >= 90:
    print('A')
elif user_grade >= 80:
    print('B')
elif user_grade >= 70:
    print('C')
elif user_grade >= 60:
    print('D')
else:
    print('F')
