def reverse(text):
    return text[::-1]

user_text = input('Type a text: ')

print('In reverse order', reverse(user_text))
