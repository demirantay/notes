from NumStat import NumStat

stats = NumStat()

stats.add_number(1)
stats.add_number(4)
stats.add_number(9)

print('Amount of numbers in stats: ', stats.amount_of_numbers)
print('Sum: ', stats.sum_of_numbers)
print('Average: ', stats.average)
