
from DecreasingCounter import *

counter = DecreasingCounter(100)

counter.print_value();
counter.decrease();
counter.print_value();

counter.decrease();
counter.print_value();

counter.reset();
counter.print_value();

counter.set_initial();
counter.print_value();
