public class Main {
  public static void main(String[] args) {
    Counter counter = new Counter(2);

    System.out.println(counter);
    counter.decrease();
    System.out.println(counter);
    counter.decrease();
    System.out.println(counter);
    counter.decrease();
    System.out.println(counter);
    counter.decrease();
    System.out.println(counter);
    counter.decrease();

  }
}
