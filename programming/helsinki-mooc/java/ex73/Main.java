public class Main {
  public static void main(String[] args) {

    Product banana = new Product("Banana", 30, 2);
    Product strawberry = new Product("Strawberry", 100, 3);

    banana.print_product();
    strawberry.print_product();

  }
}
