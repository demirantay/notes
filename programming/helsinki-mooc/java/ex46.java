public class ex46 {

  public static void main(String[] args) {
    System.out.println("Averageof 1, 2, 3, 4 : " + average(1, 2, 3, 4));
  }

  public static double average(int num1, int num2, int num3, int num4) {
    return (num1+num2+num3+num4) / 4;
  }

}
