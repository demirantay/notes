public class ex05 {
  public static void main(String[] args) {

    int seconds_in_a_hour = 60 * 60;
    int seconds_in_a_day = seconds_in_a_hour * 24;
    int seconds_in_a_year = seconds_in_a_day * 365;

    System.out.println("Seconds in a year: "+seconds_in_a_year);
  }
}
