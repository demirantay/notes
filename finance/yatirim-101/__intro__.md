## Yatirim 101 - Giris

- Bu not sayfasinda Michele Cagan tarafindan yazilmis `Yatirim 101` isimli kitaptaki notlarimi tutacagim.

### Temel Ekonomi

- Ekonominin en temel onculu sudur: Tuketiciler para harcarsa ekonomi buyueybilir, harcamazlarsa buyuyemez. Eknomi durgun oldugunda tuketici harcamlari duser, genel olarak sirketlerin buyumesi durur ve yatirimcilar dusuk getiriler elde eder. Ekonomi gelistiginge insanlar para harcar, sirketlerin isleri iyi gider ve yatirimlar artar. Ekonominin nasil isledigini, girdigi donguleri ve piyasalardaki etkilerini anlamak sizin daha basarili bir yatirimci olmaniza katkida bulunabilir.

### Alim ve Satim

- *"Akilli yatirimci iyimserlere satan ve kotumserlerden satin alan bir realisttir."* - Jason Zweig

- `Deger ve Fiyat` : hayatimizda alip sattigimiz seylerin bizler tarafindan bir degeri vardir. Her ne kadar bagzi seylerin degeri manevi yuk ten dolayi degissede belli basli kurallar coguz zaman kirilamaz haldedir. Mesela Arz artarsa fiyatlar duser. Talep artarsa fiyatlar yukselir.
- `Gelir` : Gelir ihtiyaciniz olan seyleri satin almak icin farkli kaynaklardan (isiniz, miras, yatirimlar) elde ettiginiz paradir. Emekli oldugunuzda ana gelir kaynaginizi kaybedersiniz ve hayatta kalmak icin hala bir gelir elde etmeniz gerekir. Bu noktada sosyal guvenligin katkisi olsa bile gercekten son vaktinizi iyi degerlendirmek istiyorsaniz, nasil yatirim yaptiginiz anahtar bir rol oynar.

- `Tuketim` : Kazanmak ve guzel yatirimlar yapmak istiyorsaniz tukettignizi ve giderleriniz minimum da tutmaya calisin, gereksiz harcamalardan kacinin ve en onelisi dugun masrafi, bosanma, nafaka, para odakli universiteler gibi toplumun endustiriyellesmis yapi taslarindan kesinlikle uzak durun. Gotunuze don alamassiniz.

- `Birikim ve Yatirim` : Ay sonundan ay sonuna yasamak ne yazikki gunumuz dunyasinda bir basari ama birikim yapmaya calismalisiniz. Ayriyetten sadece para biriktirerek koseyi donemessiniz unutmayin altin kural parayi kendiniz icin calistirabilmektedir

### Faiz Oranlari

