# Finance Syllabus

The course syllabus is not in final shape at the moment I will edit as I go along.

### Introduction

Do these in order however after the intro you are free to choose specializing in whatever topic you would like to specilize.

- [ ] Read: Yatirim 101
- [ ] Course: Khan Academy -- [Personal Finance](https://www.khanacademy.org/college-careers-more/personal-finance)
- [ ] Course: Khan Academy -- [Finance and Capital Markets](https://www.khanacademy.org/economics-finance-domain/core-finance)
- [ ] Course: [Finance For Everyone](https://www.coursera.org/specializations/finance-for-everyone)
- [ ] Course: [Invesment Management](https://www.coursera.org/specializations/investment-management)
- [ ] Course: [Portfolio Management](https://www.coursera.org/specializations/investment-portolio-management)

### Future syllabus topics

Try to add new courses and books for each of the topic of your choosing and change the syllabus in the future 

- [ ] Interest and Debt
- [ ] Housing
- [ ] Inflation
- [ ] Taxes
- [ ] Accounting and Financial Statements
- [ ] Stocks and Bonds
- [ ] Invesment Vehicles, Insurance and Retirement
- [ ] Money Banking and Central Banks
- [ ] Options, Swaps, futures, MBSs, CDOs and other derivatives
- [ ] Current Economics
- [ ] Micro Economics
- [ ] Macro Economics
